"""Utils module for downloader views."""
import csv
import io
from datetime import datetime

from backend.api.schemas import ListProductSchema


def format_csv_row_generator(row, columns):
    """Build and return a list of lists (header row + data rows)."""
    _row = []
    for column in columns:
        _value = row.get(column, "")
        _row.append(_value)
    return _row


def stream_csv(data, _columns):
    """Stream the CSV."""
    line = io.StringIO()
    writer = csv.writer(line)
    writer.writerow(_columns)
    line.seek(0)
    yield line.read()
    line.truncate(0)
    line.seek(0)
    for csv_line in data:
        writer.writerow(format_csv_row_generator(csv_line, _columns))
        line.seek(0)
        yield line.read()
        line.truncate(0)
        line.seek(0)


def get_filename(file_name, _type):
    timestamp = datetime.now()
    formatted_timestamp = timestamp.strftime("%Y%m%d-%H%M%S")
    return f"{file_name}_{formatted_timestamp}.{_type}"


def get_columns(exclude=None):
    """Return the columns to be used as headers in csv."""
    columns = ListProductSchema.Meta.fields.copy()
    if not exclude:
        return columns
    return [
        col
        for col in columns
        if col not in exclude
    ]
