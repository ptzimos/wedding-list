"""Unittests for downloader utils."""
from backend.api.downloader.utils import (
    format_csv_row_generator,
    get_columns,
)
from backend.api.schemas import ListProductSchema
from backend.base.testcases import TestCase


class UtilsTestCase(TestCase):

    def test_maps_correctly_the_cols_to_data(self):
        columns = ["one", "two"]
        row = {"one": "first", "two": "second"}
        result = format_csv_row_generator(row, columns)
        self.assertEqual(result, ["first", "second"])

    def test_get_columns_excludes_columns(self):
        original_cols = ListProductSchema.Meta.fields.copy()
        excluded_columns = ["name", "price", ]
        expected_excluded_cols = sorted([
            col
            for col in original_cols
            if col not in excluded_columns
        ])
        self.assertEqual(
            sorted(get_columns(excluded_columns)),
            expected_excluded_cols
        )
