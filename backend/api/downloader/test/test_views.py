"""Unittests for downloader views."""
from backend.base import factory
from backend.base.testcases import TestCase


class GetGiftListsTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.url = "/downloader/{}"

    def test_returns_401_unauthorized_on_no_auth_headers(self):
        response = self.client.get(self.url.format(1))
        self.assertEqual(401, response.status_code)

    def test_returns_400_when_user_hasnt_got_this_gift_list_related(self):
        gift_list = factory.make_gift_list()
        headers, _ = self.get_headers()
        response = self.client.get(
            self.url.format(gift_list.id),
            headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"],
            "This user hasn't got a gift list related."
        )

    def test_returns_400_when_gift_list_has_no_products(self):
        couple = factory.make_couple()
        gift_list = factory.make_gift_list(couple_id=couple.id)
        headers, _ = self.get_headers(couple.users[0])
        response = self.client.get(
            self.url.format(gift_list.id),
            headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"],
            "Not products related to this gift list."
        )

    def test_produce_a_report_for_a_given_product_successfully(self):
        couple = factory.make_couple()
        product = factory.make_product()
        gift_list = factory.make_gift_list(couple_id=couple.id)
        for _ in range(5):
            factory.make_list_product(
                product_id=product.id,
                gift_list_id=gift_list.id
            )
        headers, _ = self.get_headers(couple.users[0])
        response = self.client.get(
            self.url.format(gift_list.id),
            headers=headers
        )
        headers, row, _ = response.data.decode("utf-8").split("\r\n")
        expected_vals = {
            "brand": product.brand,
            "name": product.name,
            "price": str(product.price),
            "currency": product.currency,
            "purchased_quantity": str(0),
            "total_quantity": str(5)
        }
        self.assertEqual(200, response.status_code)
        self.assertEqual(headers.split(","), list(expected_vals.keys()))
        self.assertEqual(row.split(","), list(expected_vals.values()))
