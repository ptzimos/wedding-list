"""Downloader endpoints."""
from flask import (
    Blueprint,
    Response,
)
from flask_jwt_extended import jwt_required

from backend.api.decorators import ensure_gift_list_for_user
from backend.api.downloader.utils import (
    get_columns,
    get_filename,
    stream_csv,
)
from backend.api.models import (
    GiftList,
    ListProduct,
)
from backend.api.schemas import GiftListsSchema
from backend.base.database import db

downloader_api = Blueprint("downloader-api", __name__)


@downloader_api.route("/<gift_list_id>", methods=["get", ])
@jwt_required
@ensure_gift_list_for_user
def download_gift_list_report(gift_list_id):
    gift_list = db.session.query(
        GiftList
    ).join(
        ListProduct
    ).filter(
        GiftList.id == gift_list_id
    ).first()

    schema = GiftListsSchema()
    data = schema.dump(gift_list)
    if not data:
        return {"message": "Not products related to this gift list."}, 400

    data = data["list_products"]

    columns = get_columns(
        ["product_id", "list_product_id", "is_purchased"]
    )
    filename = get_filename("gifts_status", "csv")
    response = Response(stream_csv(data, columns))
    response.status_code = 200
    response.headers = {
        "Content-Type": "text/csv",
        "Content-Disposition": "attachment; filename={}".format(filename),
    }

    return response
