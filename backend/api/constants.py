"""Project wide constants."""
import os

from backend.api.models import (
    Couple,
    GiftList,
    ListProduct,
    Product,
    User,
)

FLASK_ENV = os.environ.get("FLASK_ENV", "dev")
BACKEND_DIR = os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))
)
PROJECT_DIR = os.path.dirname(BACKEND_DIR)
TEST_DB = "test_db"
SORTED_TABLES_MAPPING = {
    "user": User,
    "couple": Couple,
    "gift_list": GiftList,
    "product": Product,
    "product_id_seq": "",
    "list_product": ListProduct,
}
