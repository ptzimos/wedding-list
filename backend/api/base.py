"""Definition of the flask app."""
import logging
import os

from flasgger import Swagger
from flask_api import FlaskAPI
from flask_debugtoolbar import DebugToolbarExtension
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate

from backend.api.commands import (
    factory_cmd,
    helpers,
)
from backend.api.constants import (
    BACKEND_DIR,
    PROJECT_DIR,
)
from backend.api.downloader.views import downloader_api
from backend.api.gift_list.views import gift_list_api
from backend.api.products.views import products_api
from backend.api.settings import settings

from backend.base.utils import clear_logs
from backend.api.auth.views import auth_api
from backend.base.database import (
    db,
    ma,
)


def create_app(settings_object=None):
    """Create the flask app."""
    clear_logs()
    log_format_str = "[%(asctime)s] %(levelname)s - %(message)s"
    logging.basicConfig(
        format=log_format_str,
        filename=os.path.join(PROJECT_DIR, "api.logs"),
        level=logging.DEBUG
    )

    app = FlaskAPI("api")

    app.config.from_object(settings_object or settings)

    # Commands registrations.
    app.register_blueprint(factory_cmd)
    app.register_blueprint(helpers)

    # Api endpoints registrations.
    app.register_blueprint(auth_api, url_prefix="/auth")
    app.register_blueprint(gift_list_api, url_prefix="/gift-list")
    app.register_blueprint(products_api, url_prefix="/products")
    app.register_blueprint(downloader_api, url_prefix="/downloader")

    db.init_app(app)
    ma.init_app(app)

    DebugToolbarExtension(app)

    JWTManager(app)
    Migrate(app, db, directory=os.path.join(BACKEND_DIR, "migrations"))
    Swagger(app)

    return app


app = create_app()


@app.route("/is-alive", methods=["get", ])
def is_alive():
    return ""
