from sqlalchemy import (
    CheckConstraint,
    Column,
    ForeignKey,
    String,
)
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy.sql import expression
from sqlalchemy_utils import PasswordType

from backend.api.models import Base


class User(Base):
    """User model definition."""
    __tablename__ = "user"

    first_name = Column(
        type_=String(255),
        nullable=True,
    )
    last_name = Column(
        type_=String(255),
        nullable=True,
    )
    email = Column(
        CheckConstraint("length(email)<=128"),
        type_=String(128),
        unique=True,
        nullable=False
    )
    password = Column(
        type_=PasswordType(schemes=["pbkdf2_sha512", "bcrypt"]),
        default=None,
        server_default=expression.null(),
        nullable=True,
    )
    couple_id = Column(
        UUID,
        ForeignKey("couple.id", ondelete="SET NULL"),
        nullable=True
    )
    couple = relationship(
        "Couple",
        backref="users",
    )
