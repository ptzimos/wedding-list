"""Initialize the couple model."""
from backend.api.models import Base


class Couple(Base):
    """Couple model definition."""
    __tablename__ = "couple"
