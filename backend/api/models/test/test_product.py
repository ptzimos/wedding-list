from backend.base import factory
from backend.base.testcases import TestCase


class ProductModelTestCase(TestCase):

    def test_can_create_product(self):
        factory.make_product()

    def test_can_set_price_when_its_integer(self):
        product = factory.make_product()
        product.price = "123"
        self.session.add(product)
        self.session.commit()

    def test_can_set_price_when_its_float(self):
        product = factory.make_product()
        product.price = "123.213"
        self.session.add(product)
        self.session.commit()

    def test_can_set_price_when_its_mixed_with_currency(self):
        product = factory.make_product()
        price = "123232.123"
        code = "AAA"
        product.price = f"{price}{code}"
        self.session.add(product)
        self.session.commit()
        self.assertEqual(float(price), product.price)
        self.assertEqual(code, product.currency)
