"""Initialize the Product model."""
from sqlalchemy import (
    Column,
    Float,
    Integer,
    String,
    and_,
)
from sqlalchemy.ext.hybrid import hybrid_property

from backend.api.models import (
    Base,
    GiftList,
)
from backend.base.database import db


class Product(Base):
    """Product model definition."""
    __tablename__ = "product"
    id = Column(
        type_=Integer(),
        primary_key=True,
        autoincrement=True,
        nullable=False,
    )
    name = Column(
        type_=String(),
        unique=True,
        nullable=False,
    )
    brand = Column(
        type_=String(),
        nullable=False,
    )
    _price = Column("price", type_=Float())
    _currency = Column("currency", type_=String(length=3), )
    in_stock_quantity = Column(
        type_=Integer(),
        default=0,
    )
    total_purchased_quantity = Column(
        type_=Integer(),
        default=0,
    )

    @hybrid_property
    def price(self):
        """Return the value of the price."""
        return self._price

    @price.setter
    def price(self, val):
        """Set the value of the price and the currency."""
        try:
            self._price = float(val)
        except ValueError:
            currency_code = val[-3:]
            self._price = val[:val.index(currency_code)]
            self._currency = currency_code

    @hybrid_property
    def currency(self):
        """Return the value of the currency."""
        return self._currency

    @currency.setter
    def currency(self, val):
        """Set the value of the currency."""
        self._currency = val

    def non_purchased_list_products(self, gift_list_id):
        """Return all the non purchased list_products for the given
         gift list."""
        from backend.api.models import ListProduct
        if not self:
            return None
        return db.session.query(
            ListProduct
        ).filter(
            and_(
                ListProduct.is_purchased.is_(False),
                self.__class__.id == self.id,
                GiftList.id == gift_list_id
            )
        ).all()
