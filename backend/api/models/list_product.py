"""ListProduct model definition."""

from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    INTEGER,
)
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import (
    backref,
    relationship,
)

from backend.api.models import Base


class ListProduct(Base):
    """Define the ListProduct table."""
    __tablename__ = "list_product"

    gift_list_id = Column(
        UUID,
        ForeignKey("gift_list.id", ondelete="cascade"),
        nullable=False
    )
    product_id = Column(
        INTEGER,
        ForeignKey("product.id", ondelete="cascade"),
        nullable=False
    )
    is_purchased = Column(
        Boolean,
        default=False,
    )

    gift_list = relationship(
        "GiftList",
        backref=backref("list_products", cascade="all,delete-orphan"),
    )
    product = relationship(
        "Product",
        backref=backref("list_products", cascade="all,delete-orphan"),
    )
