"""GiftList model definition."""
from sqlalchemy import (
    Column,
    ForeignKey,
)
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from backend.api.models import Base


class GiftList(Base):
    """Defining the gift list of a couple."""
    __tablename__ = "gift_list"
    couple_id = Column(
        UUID,
        ForeignKey("couple.id", ondelete="cascade"),
        nullable=True
    )
    couple = relationship(
        "Couple",
        backref="gift_lists",
    )

    def add_products(self, product_ids):
        if isinstance(product_ids, str):
            product_ids = [product_ids, ]
