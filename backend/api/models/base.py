"""Base declarative class for the models defined."""
import uuid

from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import force_auto_coercion

registry = dict()
_Base = declarative_base(class_registry=registry)

force_auto_coercion()


class Base(_Base):
    __abstract__ = True

    id = Column(
        type_=UUID,
        primary_key=True,
        default=lambda: str(uuid.uuid4()),
        nullable=False,
    )
