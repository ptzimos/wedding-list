"""The couple schemas."""
from marshmallow_sqlalchemy import ModelSchema

from backend.api.models import Couple


class CoupleSchema(ModelSchema):
    class Meta:
        model = Couple
