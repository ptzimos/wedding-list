"""The list_product schemas."""
from marshmallow import (
    fields,
    post_dump,
)
from marshmallow_sqlalchemy import (
    ModelSchema,
)

from backend.api.models import ListProduct


class ListProductSchema(ModelSchema):
    list_product_id = fields.UUID(attribute="id", dump_only=True)
    name = fields.Method("get_name")
    brand = fields.Method("get_brand")
    price = fields.Method("get_price")
    currency = fields.Method("get_currency")
    total_quantity = fields.Integer(default=0)
    purchased_quantity = fields.Integer(default=0)

    class Meta:
        model = ListProduct
        fields = [
            "list_product_id",
            "brand",
            "name",
            "price",
            "currency",
            "purchased_quantity",
            "total_quantity",
            "product_id",
            "is_purchased",
        ]

    def get_name(self, obj):
        """Return the name of the product."""
        return obj.product.name

    def get_brand(self, obj):
        """Return the brand of the product."""
        return obj.product.brand

    def get_price(self, obj):
        """Return the price of the product."""
        return obj.product.price

    def get_currency(self, obj):
        """Return the currency of the product."""
        return obj.product.currency

    def find_in_list(self, _list, element):
        """Return the indef the list if a product_id exists already in
        that list"""
        for idx, elem in enumerate(_list):
            if elem["product_id"] == element["product_id"]:
                return idx
        return None

    @post_dump(pass_many=True)
    def adapt_quantities_for_data(self, data, many, **kwargs):
        """Define the total_quantity and the purchased quantity for a product
        in a gift list."""
        new_data = []
        for element in data:
            idx = self.find_in_list(new_data, element)

            _element = element if idx is None else new_data[idx]
            _element["total_quantity"] += 1
            if element["is_purchased"]:
                _element["purchased_quantity"] += 1
            if idx is None:
                new_data.append(element)

        return new_data
