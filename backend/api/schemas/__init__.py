"""initialization file for schemas package."""
from backend.api.schemas.auth import UserLoginSchema
from backend.api.schemas.couple import CoupleSchema
from backend.api.schemas.product import (
    AmendProductsSchema,
    ProductSchema,
)
from backend.api.schemas.list_product import ListProductSchema
from backend.api.schemas.gift_list import (
    AmendProductForGiftListSchema,
    CreateGiftListSchema,
    GiftListsSchema,
)

__all__ = [
    "AmendProductsSchema",
    "AmendProductForGiftListSchema",
    "CoupleSchema",
    "CreateGiftListSchema",
    "GiftListsSchema",
    "ListProductSchema",
    "ProductSchema",
    "UserLoginSchema",
]
