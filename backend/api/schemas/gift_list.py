"""The gift_list schemas."""
from marshmallow import (
    ValidationError,
    fields,
    pre_load,
    validates_schema,
)
from marshmallow_sqlalchemy import ModelSchema

from backend.api.models import (
    GiftList,
    ListProduct,
)
from backend.api.schemas import (
    AmendProductsSchema,
    ListProductSchema,
)
from backend.base.database import (
    db,
    ma,
)


class GiftListsSchema(ModelSchema):
    list_products = fields.Nested(nested=ListProductSchema, many=True)

    class Meta:
        model = GiftList
        fields = [
            "id",
            "list_products",
        ]


class CreateGiftListSchema(ma.Schema):
    couple_id = fields.UUID(load_only=True, required=True)
    products = fields.Nested(AmendProductsSchema, many=True)

    class Meta:
        fields = [
            "couple_id",
            "products",
        ]

    @staticmethod
    def save(data):
        """Create a gift list with ListProduct instances attached."""
        gift_list = GiftList(couple_id=str(data["couple_id"]))
        list_products = [
            ListProduct(gift_list=gift_list, product_id=product["id"])
            for product in data["products"]
        ]
        db.session.add_all([gift_list, *list_products])
        db.session.commit()
        return gift_list.id


class AmendProductForGiftListSchema(ma.Schema):
    errors = {
        "gift_list_not_found": "Gift list is not found.",
    }
    gift_list_id = fields.UUID(required=True, load_only=True)
    products = fields.Nested(AmendProductsSchema, many=True, strict=True)
    action = fields.String()

    class Meta:
        fields = [
            "gift_list_id",
            "products",
            "action",
        ]

    @pre_load(pass_many=True)
    def set_context(self, data, many, **kwargs):
        """Set the context to be available to the nested schema as well."""
        self.context["action"] = data["action"]
        return data

    @validates_schema(pass_many=True)
    def validate_schema(self, data, many, **kwargs):
        """Validate schema."""
        gift_list = db.session.query(
            GiftList
        ).filter(
            GiftList.id == str(data["gift_list_id"])
        ).first()
        self.context["gift_list"] = gift_list
        if not gift_list:
            raise ValidationError(
                self.errors["gift_list_not_found"], "gift_list_id")

    def save(self, data):
        """Add products in a gift list."""
        gift_list = self.context["gift_list"]
        products = data["products"]
        list_products = []
        for product in products:
            for times in range(product["amount"]):
                list_products.append(
                    ListProduct(gift_list=gift_list, product_id=product["id"])
                )
        db.session.add_all([gift_list, *list_products])
        db.session.commit()

        return gift_list.id

    def delete(self, data):
        """Delete products from a gift list."""
        products = data["products"]
        product_ids = [prod["id"] for prod in products]
        db.session.query(ListProduct).filter(
            ListProduct.product_id.in_(product_ids)
        ).delete(synchronize_session="fetch")
        return self.context["gift_list"].id
