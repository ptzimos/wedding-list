"""Define the Product Schemas."""
from marshmallow import (
    ValidationError,
    fields,
    validates_schema,
)
from marshmallow_sqlalchemy import ModelSchema
from sqlalchemy.exc import DataError

from backend.api.enums import ProductActionEnum
from backend.api.models import Product
from backend.base.database import (
    db,
    ma,
)


class ProductSchema(ModelSchema):
    class Meta:
        model = Product
        fields = [
            "id",
            "name",
            "brand",
            "price",
            "currency",
            "in_stock_quantity",
            "total_purchased_quantity",
        ]


class AmendProductsSchema(ma.Schema):
    errors = {
        "non_equal_prod_ids": (
            "The product ids that you have provided are not in the database"
        ),
        "invalid_data_provided": (
            "You have provided invalid data. Please try again."
        ),
        "duplicates_found":
            "You have provided duplicated products. Please try again.",
        "not_in_stock": "{} product is not in stock.",
        "not_negative_allowed": "Amounts cannot be negative. Please try again."
    }
    id = fields.Integer(required=True)
    amount = fields.Integer(required=True)

    @validates_schema(pass_many=True)
    def validate_schema(self, data, many, **kwargs):
        """Schema validation."""
        products = data
        _product_ids = [prod["id"] for prod in products]
        product_ids = list(set(_product_ids))
        if len(_product_ids) != len(product_ids):
            raise ValidationError(self.errors["duplicates_found"], "id")

        try:
            products_from_db = db.session.query(
                Product
            ).filter(
                Product.id.in_(product_ids)
            ).all()
        except DataError:
            raise ValidationError(self.errors["invalid_data_provided"], "id")

        if len(products_from_db) != len(product_ids):
            raise ValidationError(self.errors["non_equal_prod_ids"], "id")

        sorted_lists = zip(
            sorted(products, key=lambda x: x["id"]),
            sorted(products_from_db, key=lambda x: x.id)
        )
        action = self.context.get("action")
        if not action:
            return
        else:
            for prod in sorted_lists:
                demanded_amount = int(prod[0]["amount"])
                supplied_amount = prod[1].in_stock_quantity
                # When the action is not existent in the context, it means
                # that a new empty gift list is created and we dont need to do
                # the following check.
                if (
                        demanded_amount >= supplied_amount and
                        action == ProductActionEnum.add.value
                ):
                    raise ValidationError(
                        self.errors["not_in_stock"].format(prod[1].name),
                        "amount"
                    )
                elif (
                        demanded_amount == 0 and
                        action == ProductActionEnum.remove.value
                ):
                    raise ValidationError(
                        self.errors["not_negative_allowed"].format(
                            prod[1].name),
                        "amount"
                    )
