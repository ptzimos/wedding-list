"""Auth schemas."""
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
)
from marshmallow import (
    ValidationError,
    fields,
    validates_schema,
)
from marshmallow.validate import (
    Email,
    Length,
)

from backend.base.database import (
    db,
    ma,
)
from backend.api.models import User


class UserLoginSchema(ma.Schema):
    """Schema class used to validate and serialize user login data."""

    errors = {
        "user_not_exist": "A user with this email does not exist.",
        "wrong_password": "Your password is wrong. Please try again.",
    }

    class Meta:
        include = {
            "email": fields.Email(
                load_only=True,
                validate=[Email(), ]
            ),
            "password": fields.String(
                load_only=True,
                validate=[Length(min=5), ]
            ),
            "access_token": fields.Method(serialize="get_access_token"),
            "refresh_token": fields.Method(serialize="get_refresh_token"),
        }

    def get_access_token(self, obj):
        """Return the access token."""
        return create_access_token(identity=obj["email"])

    def get_refresh_token(self, obj):
        """Return the refresh token."""
        return create_refresh_token(identity=obj["email"])

    @validates_schema
    def validate_email_and_password_correct(self, data, **kwargs):
        """Validate provided credentials.

        First, find the user and if he exists then check if the password is
        the correct one.
        """
        user = db.session.query(
            User
        ).filter(
            User.email == data.get("email", "")
        ).one_or_none()
        if not user:
            raise ValidationError(self.errors["user_not_exist"])
        if user.password != data["password"]:
            raise ValidationError(self.errors["wrong_password"])
