"""Settings for initializing the flask app."""
import os

from backend.api.constants import FLASK_ENV


class BaseConfig:
    """Base config from which the rest conig modes derive."""
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = "V459kzk8OJh0zi2JhmJZaeg9w1-Pl4IB7TTwc7QUAtC3EW1qTh"
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
    SQLALCHEMY_BINDS = {
        "test_db": "postgresql://postgres:password@localhost:5428/test_db",
    }
    SWAGGER = {
        "title": "Wedding shop api",
        "uiversion": 3,
    }


class DevConfig(BaseConfig):
    """Development configuration class."""
    DEBUG = True
    DEBUG_TB_ENABLED = True
    TESTING = False
    CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = (
        "postgresql://postgres:password@localhost:5428/postgres"
    )


class TestingConfig(BaseConfig):
    """Development configuration class."""
    DEBUG = True
    TESTING = True
    CSRF_ENABLED = False
    SQLALCHEMY_DATABASE_URI = (
        "postgresql://postgres:password@localhost:5428/test_db"
    )


SETTINGS_MAPPING = {
    "dev": DevConfig,
    "testing": TestingConfig,
}

# Get the settings object to be used in the flask app.
settings = SETTINGS_MAPPING[FLASK_ENV]()
