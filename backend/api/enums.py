"""Enums that can be used in the api."""
from enum import Enum


class ProductActionEnum(Enum):
    add = "add"
    remove = "remove"
