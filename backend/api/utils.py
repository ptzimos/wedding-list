"""Generic utils for the api."""


def parse_errors(messages, kept_errors=None):
    """Return all the errors in a flat list structure."""
    kept_msg = [] if kept_errors is None else kept_errors
    if isinstance(messages, list):
        for elem in messages:
            if isinstance(elem, dict):
                kept_errors += parse_errors(
                    elem, kept_errors=kept_errors)
            elif isinstance(elem, str):
                kept_msg.append(elem)
    if isinstance(messages, dict):
        for k, v in messages.items():
            return parse_errors(v, kept_errors)
    return kept_msg
