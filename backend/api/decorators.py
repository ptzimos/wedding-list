from functools import wraps

from flask_api.exceptions import ParseError
from flask_jwt_extended import get_jwt_identity
from sqlalchemy import and_

from backend.api.models import GiftList
from backend.base.database import db
from backend.base.utils import get_current_user


def ensure_gift_list_for_user(func):
    """Raise an error if the there is not a gift list for this user
       and the current gift_list_id.
    """

    @wraps(func)
    def wrapped_function(*args, **kwargs):
        gift_list_id = kwargs.get("gift_list_id")
        user = get_current_user(get_jwt_identity(), db.session)
        if not user.couple_id:
            raise ParseError("This user hasn't got a gift list related.")
        gift_list_exists_for_user = db.session.query(
            db.exists().where(
                and_(
                    GiftList.couple_id == user.couple_id,
                    GiftList.id == gift_list_id
                )
            )
        ).scalar()
        if not gift_list_exists_for_user:
            raise ParseError("The chosen gift list doesn't exist.")
        return func(*args, **kwargs)

    return wrapped_function
