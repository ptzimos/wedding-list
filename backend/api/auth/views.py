"""Authentication endpoints module."""
from flasgger import swag_from
from flask import (
    Blueprint,
    request,
)
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    jwt_refresh_token_required,
)
from marshmallow import ValidationError

from backend.api.schemas import UserLoginSchema
from backend.api.utils import parse_errors

auth_api = Blueprint("auth", __name__)


@auth_api.route("/login", methods=["post"])
@swag_from("docs/login.yaml")
def login():
    """Login the user and return the access token."""
    schema = UserLoginSchema()

    try:
        schema.load(request.data)
    except ValidationError as err:
        return {"message": parse_errors(err.messages)}, 400

    return schema.dump(request.data)


@auth_api.route("/refresh-token", methods=["POST"])
@jwt_refresh_token_required
@swag_from("docs/refresh_token.yaml")
def refresh_token():
    """Generate new tokens from the old refresh token."""
    user_email = get_jwt_identity()
    return {
        "access_token": create_access_token(identity=user_email),
        "refresh_token": create_refresh_token(identity=user_email)
    }
