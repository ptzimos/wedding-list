"""Auth views test case."""
from unittest.mock import MagicMock

from flask_jwt_extended import create_refresh_token

from backend.api.schemas import auth
from backend.api.auth import views
from backend.api.schemas import UserLoginSchema
from backend.base.factory import make_user
from backend.base.testcases import TestCase


class LoginViewTestCase(TestCase):

    def setUp(self):
        super().setUp()
        self.payload = {"email": "admin@admin.com", "password": "password"}
        self.url = "/auth/login"

    def test_user_does_not_exist_in_the_db_returns_400_code(self):
        response = self.client.post(self.url, data=self.payload)
        expected_error = UserLoginSchema.errors["user_not_exist"]
        actual_error = response.json["message"][0]
        self.assertEqual(400, response.status_code)
        self.assertEqual(expected_error, actual_error)

    def test_returns_access_code_and_200_code_if_user_provides_correct_creds(
            self):
        expected_access_token = "access_token"
        expected_refresh_token = "refresh_token"
        make_user(**self.payload, session=self.session)
        self.patch(
            auth,
            "create_access_token",
            MagicMock(return_value=expected_access_token)
        )
        self.patch(
            auth,
            "create_refresh_token",
            MagicMock(return_value=expected_refresh_token)
        )
        response = self.client.post(self.url, data=self.payload)

        self.assertEqual(200, response.status_code)
        self.assertEqual(expected_access_token, response.json["access_token"])
        self.assertEqual(
            expected_refresh_token, response.json["refresh_token"])

    def test_returns_400_when_wrong_password_provided_for_correct_email(self):
        wrong_password_data = {
            "email": self.payload["email"],
            "password": "blahhhh"
        }
        make_user(**self.payload, session=self.session)
        response = self.client.post(self.url, data=wrong_password_data)

        self.assertEqual(400, response.status_code)
        self.assertEqual(
            UserLoginSchema.errors["wrong_password"],
            response.json["message"][0]
        )


class RefreshTokenTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.url = "/auth/refresh-token"

    def test_returns_new_tokens_when_correct_refresh_token_is_provided(self):
        user = make_user()
        refresh_token = create_refresh_token(identity=user.email)
        headers = {"Authorization": f"Bearer {refresh_token}"}
        expected_new_refresh_token = "new_refresh_token"
        expected_new_access_token = "new_access_token"

        self.patch(
            views,
            "create_access_token",
            MagicMock(return_value=expected_new_access_token)
        )

        self.patch(
            views,
            "create_refresh_token",
            MagicMock(return_value=expected_new_refresh_token)
        )
        response = self.client.post(self.url, headers=headers)
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            expected_new_refresh_token,
            response.json["refresh_token"]
        )
        self.assertEqual(
            expected_new_access_token,
            response.json["access_token"]
        )

    def test_return_422_when_the_jwt_is_wrong(self):
        headers = {"Authorization": "Bearer blah.s.s"}
        response = self.client.post(self.url, headers=headers)
        self.assertEqual(422, response.status_code)

    def test_returns_401_when_auth_header_is_missing(self):
        response = self.client.post(self.url)
        self.assertEqual(401, response.status_code)
