"""Commands for creating objects in the db."""
import sys

import click
from flask import Blueprint
from backend.base.factory import (
    make_couple,
    make_user,
)

factory_cmd = Blueprint("factory", __name__)


@factory_cmd.cli.command("create-user")
@click.argument("email", required=False)
@click.argument("password", required=False)
def create_user(email=None, password=None):
    """Create user in the db, with optional give email and password."""
    if not email:
        email = "admin@admin.com"
    if not password:
        password = "password"
    sys.stdout.write(
        f"Creating user with the following credentials:"
        f"\n\temail: {email}\n\tpassword: {password}\n"
    )
    make_user(email=email, password=password)
    sys.stdout.write("User created successfully.\n")


@factory_cmd.cli.command("create-couple")
@click.argument("email1", required=False)
@click.argument("email2", required=False)
def create_couple(email1=None, email2=None):
    """Create couple made by two users"""
    password = "password"
    users = [
        make_user(email=email, password=password)
        for email in [email1, email2]
    ]
    sys.stdout.write(
        f"Creating couple with the following credentials.\n\t"
        f"email: {users[0].email}\n\tpassword: {password}\n\t"
        f"email: {users[1].email}\n\tpassword: {password}\n"
    )
    make_couple(users=users)
    sys.stdout.write("Created couple successfully.")
