"""Generic helper commands."""
import json
import os
import subprocess
import sys

import click
import sqlalchemy
from flask import Blueprint

from backend.api.constants import (
    PROJECT_DIR,
    SORTED_TABLES_MAPPING,
    TEST_DB,
)
from backend.api.settings import DevConfig
from backend.base.database import db
from backend.base.utils import cleanup_db

helpers = Blueprint("helpers", __name__)


def create_db():
    """Check to see if the testing DB exists, and create it if not."""
    # Ensure that the DB is around to test on.
    engine = sqlalchemy.create_engine(
        DevConfig.SQLALCHEMY_DATABASE_URI,
        isolation_level="AUTOCOMMIT")
    engine.execute(f"DROP DATABASE IF EXISTS {TEST_DB}")
    engine.execute(f"CREATE DATABASE {TEST_DB}")


def run_migrations():
    """Make migrations in testing db."""
    success = True
    cmd = ["flask", "db", "upgrade"]
    alembic_process = subprocess.Popen(
        cmd, stdout=sys.stdout, stderr=sys.stderr)
    alembic_process.communicate()
    success = success and (alembic_process.returncode == 0)
    return success


@helpers.cli.command("create-test-db")
def create_testing_db():
    """Create the test db."""
    sys.stderr.write("Creating test_db...\n")
    create_db()
    sys.stderr.write(f"{TEST_DB} database created successfully.")
    sys.stderr.write(f"Running migrations for {TEST_DB}...\n")
    success = run_migrations()
    if success:
        sys.stderr.write("Migrations completed\n")
    else:
        sys.stderr.write("Migrations failed to apply.\n")


@helpers.cli.command("cleanup-db")
def _cleanup_db():
    """Delete all objects from the database tables."""
    sys.stderr.write("Deleting all objects from database tables...\n")
    cleanup_db(db.session)
    sys.stderr.write("Clean up finished successfully.\n")


@helpers.cli.command("import-db-json-blobs")
@click.argument("table", required=True)
def import_db_blobs(table=None):
    """Import db objects from json blobs."""
    if not table:
        raise ValueError("You must specify a table to load the blobs.")
    file = os.path.join(PROJECT_DIR, "db_blobs", f"{table}.json")

    sys.stdout.write(
        f"Starting importing {table} objects from {file}\n"
    )
    with open(file) as f:
        data = json.load(f)
    model = SORTED_TABLES_MAPPING.get(table)
    if not model:
        raise ValueError(
            "Please specify a json file which has similar name with a "
            "db table.\n"
        )
    db_data = [
        model(**elem)
        for elem in data
    ]
    db.session.add_all(db_data)
    db.session.commit()
    sys.stdout.write(
        f"Importing blobs finished succesfully, "
        f"creating {len(db_data)} items in {table} table.\n"
    )
