import json

from backend.api.models import (
    GiftList,
    Product,
)
from backend.api.schemas.gift_list import AmendProductsSchema
from backend.base import factory
from backend.base.testcases import TestCase


class GetGiftListsTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.url = "/gift-list"

    def test_returns_401_unauthorized_on_no_auth_headers(self):
        response = self.client.get(self.url + "/")
        self.assertEqual(401, response.status_code)

    def test_returns_empty_dict_when_couple_hasnt_got_gift_list_related(self):
        couple = factory.make_couple()
        user = couple.users[0]
        headers = self.create_jwt_header_for_user(user.email)
        response = self.client.get(self.url + "/", headers=headers)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json, [])

    def test_returns_gift_list_with_list_products_for_given_couple(self):
        couple = factory.make_couple()
        user = couple.users[0]
        gift_list = factory.make_gift_list(couple_id=couple.id)
        products = [factory.make_product() for _ in range(5)]
        list_products = [
            factory.make_list_product(
                product_id=product.id, gift_list_id=gift_list.id)
            for product in products
        ]
        list_products_ids = [lp.id for lp in list_products]

        headers = self.create_jwt_header_for_user(user.email)
        response = self.client.get(self.url + "/", headers=headers)
        data = response.json
        actual_gift_list = data[0]
        actual_list_product_keys = actual_gift_list["list_products"][0].keys()
        expected_gift_list_keys = ["id", "list_products"]
        expected_list_product_keys = [
            "list_product_id",
            "name",
            "brand",
            "price",
            "currency",
            "total_quantity",
            "product_id",
            "is_purchased",
            "purchased_quantity"
        ]
        self.assertEqual(200, response.status_code)
        self.assertIsInstance(data, list)
        self.assertEqual(len(data), 1)
        self.assertIsInstance(actual_gift_list, dict)
        self.assertEqual(
            expected_gift_list_keys,
            sorted(actual_gift_list.keys())
        )
        self.assertEqual(
            sorted(expected_list_product_keys),
            sorted(actual_list_product_keys)
        )
        self.assertEqual(actual_gift_list["id"], gift_list.id)
        self.assertIsInstance(actual_gift_list["list_products"], list)
        self.assertEqual(len(actual_gift_list["list_products"]), 5)
        for lp in actual_gift_list["list_products"]:
            self.assertIn(lp["list_product_id"], list_products_ids)

    def test_returns_the_correct_total_quantity_for_some_products(self):
        couple = factory.make_couple()
        user = couple.users[0]
        gift_list = factory.make_gift_list(couple_id=couple.id)
        product1, product2 = [factory.make_product() for _ in range(2)]
        first_prod_list_products_num = 5
        second_prod_list_products_num = 3
        for _ in range(first_prod_list_products_num):
            factory.make_list_product(
                product_id=product1.id, gift_list_id=gift_list.id)
        for _ in range(second_prod_list_products_num):
            factory.make_list_product(
                product_id=product2.id, gift_list_id=gift_list.id)
        headers = self.create_jwt_header_for_user(user.email)
        response = self.client.get(self.url + "/", headers=headers)
        expected_list_products = sorted(
            response.json[0]["list_products"],
            key=lambda x: x["product_id"]
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(len(response.json), 1)
        self.assertEqual(len(expected_list_products), 2)
        self.assertEqual(
            len(product1.list_products),
            expected_list_products[0]["total_quantity"]
        )
        self.assertEqual(
            first_prod_list_products_num,
            expected_list_products[0]["total_quantity"]
        )
        self.assertEqual(
            len(product2.list_products),
            expected_list_products[1]["total_quantity"]
        )
        self.assertEqual(
            second_prod_list_products_num,
            expected_list_products[1]["total_quantity"]
        )


class CreateGiftListTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.url = "/gift-list/create"

    def test_returns_401_unauthorized_on_no_auth_headers(self):
        response = self.client.post(self.url)
        self.assertEqual(401, response.status_code)

    def test_raises_error_when_the_product_ids_are_malformed(self):
        couple = factory.make_couple()
        data = {
            "couple_id": couple.id,
            "products": [
                {"id": "2asdsad", "amount": 123}, {"id": 2, "amount": 3}
            ]
        }
        headers, _ = self.get_headers(couple.users[0])
        response = self.client.post(
            self.url, data=json.dumps(data), headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"][0],
            "Not a valid integer."
        )

    def test_raises_error_when_duplicated_product_ids_provided(self):
        couple = factory.make_couple()
        data = {
            "couple_id": couple.id,
            "products": [
                {"id": 2, "amount": 123}, {"id": 2, "amount": 3},
            ]
        }
        headers, _ = self.get_headers(couple.users[0])
        response = self.client.post(
            self.url, data=json.dumps(data), headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"][0],
            AmendProductsSchema.errors["duplicates_found"]
        )

    def test_raises_error_when_the_product_ids_contain_more_ids_than_in_db(
            self):
        couple = factory.make_couple()
        product = factory.make_product(in_stock_quantity=10)
        another_id = self.session.query(
            Product.id
        ).order_by(
            Product.id.desc()
        ).first()[0] + 10
        data = {
            "couple_id": couple.id,
            "products": [
                {"id": product.id, "amount": 1},
                {"id": another_id, "amount": 1}
            ]
        }
        headers, _ = self.get_headers(couple.users[0])
        response = self.client.post(
            self.url, data=json.dumps(data), headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"][0],
            AmendProductsSchema.errors["non_equal_prod_ids"]
        )

    def test_creates_a_gift_instance_based_on_the_given_products(self):
        product_instances = [factory.make_product() for _ in range(10)]
        products = [
            {"id": pr.id, "amount": pr.in_stock_quantity - 2}
            for pr in product_instances
        ]
        product_ids = [prod["id"] for prod in products]
        couple = factory.make_couple()
        data = {
            "couple_id": str(couple.id),
            "products": products
        }
        headers, _ = self.get_headers(couple.users[0])
        response = self.client.post(
            self.url, data=json.dumps(data), headers=headers
        )
        gift_list = self.session.query(
            GiftList
        ).filter(
            GiftList.id == response.json["gift_list_id"]
        ).first()
        expected_product_ids = sorted([
            lp.product_id
            for lp in gift_list.list_products
        ])
        self.assertEqual(10, len(gift_list.list_products))
        self.assertEqual(expected_product_ids, sorted(product_ids))
        self.assertEqual(gift_list.couple, couple)
        self.assertEqual(200, response.status_code)


class AddProductsToGiftListTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.url = "/gift-list/{gift_list_id}/add-products"

    def test_returns_401_unauthorized_on_no_auth_headers(self):
        response = self.client.patch(self.url.format(gift_list_id=1))
        self.assertEqual(401, response.status_code)

    def test_raises_error_when_the_product_ids_are_malformed(self):
        couple = factory.make_couple()
        gift_list = factory.make_gift_list(couple_id=couple.id)
        data = {
            "gift_list_id": gift_list.id,
            "products": [
                {"id": "2asdsad", "amount": 123}, {"id": 2, "amount": 3}
            ]
        }
        headers, _ = self.get_headers(couple.users[0])
        response = self.client.patch(
            self.url.format(gift_list_id=str(gift_list.id)),
            data=json.dumps(data), headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"][0],
            "Not a valid integer."
        )

    def test_raises_error_when_duplicated_product_ids_provided(self):
        couple = factory.make_couple()
        user = couple.users[0]
        gift_list = factory.make_gift_list(couple_id=str(couple.id))
        data = {
            "gift_list_id": str(gift_list.id),
            "products": [
                {"id": 2, "amount": 123}, {"id": 2, "amount": 3},
            ]
        }
        headers, _ = self.get_headers(user)
        response = self.client.patch(
            self.url.format(gift_list_id=str(gift_list.id)),
            data=json.dumps(data), headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"][0],
            AmendProductsSchema.errors["duplicates_found"]
        )

    def test_raises_error_when_the_product_ids_contain_more_ids_than_in_db(
            self):
        couple = factory.make_couple()
        gift_list = factory.make_gift_list(couple_id=couple.id)
        product = factory.make_product(in_stock_quantity=10)
        another_id = self.session.query(
            Product.id
        ).order_by(
            Product.id.desc()
        ).first()[0] + 10
        data = {
            "gift_list_id": gift_list.id,
            "products": [
                {"id": product.id, "amount": 1},
                {"id": another_id, "amount": 1}
            ]
        }
        headers, _ = self.get_headers(couple.users[0])
        response = self.client.patch(
            self.url.format(gift_list_id=str(gift_list.id)),
            data=json.dumps(data), headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"][0],
            AmendProductsSchema.errors["non_equal_prod_ids"]
        )

    def test_raises_when_a_user_tries_to_add_to_another_list(self):
        couple = factory.make_couple()
        product = factory.make_product()
        gift_list = factory.make_gift_list(couple_id=couple.id)
        data = {
            "gift_list_id": gift_list.id,
            "products": [
                {"id": product.id, "amount": 1},
            ]
        }
        headers, _ = self.get_headers()
        response = self.client.patch(
            self.url.format(gift_list_id=gift_list.id),
            data=json.dumps(data), headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"],
            "This user hasn't got a gift list related."
        )

    def test_adds_one_product_to_gift_list(self):
        couple = factory.make_couple()
        user = couple.users[0]
        product = factory.make_product()
        gift_list = factory.make_gift_list(couple_id=couple.id)
        data = {
            "gift_list_id": gift_list.id,
            "products": [
                {"id": product.id, "amount": 1},
            ]
        }
        headers, _ = self.get_headers(user)
        response = self.client.patch(
            self.url.format(gift_list_id=gift_list.id),
            data=json.dumps(data), headers=headers
        )
        created_list_products = gift_list.list_products

        self.assertEqual(response.json["gift_list_id"], gift_list.id)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, len(created_list_products))
        self.assertEqual(created_list_products[0].product_id, product.id)

    def test_adds_many_products_to_gift_list(self):
        couple = factory.make_couple()
        user = couple.users[0]
        products = [factory.make_product() for _ in range(10)]

        products_payload = [
            {"id": product.id, "amount": 1}
            for product in products
        ]
        gift_list = factory.make_gift_list(couple_id=couple.id)
        data = {
            "gift_list_id": gift_list.id,
            "products": products_payload
        }
        headers, _ = self.get_headers(user)
        response = self.client.patch(
            self.url.format(gift_list_id=gift_list.id),
            data=json.dumps(data), headers=headers
        )
        created_list_products = gift_list.list_products
        expected_added_product_ids = sorted([
            product.id for product in products])
        actual_added_product_ids = sorted([
            list_product.product_id for list_product in created_list_products
        ])

        self.assertEqual(response.json["gift_list_id"], gift_list.id)
        self.assertEqual(200, response.status_code)
        self.assertEqual(10, len(created_list_products))
        self.assertEqual(actual_added_product_ids, expected_added_product_ids)


class RemoveProductsToGiftListTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.url = "/gift-list/{gift_list_id}/remove-products"

    def test_returns_401_unauthorized_on_no_auth_headers(self):
        response = self.client.delete(self.url.format(gift_list_id=1))
        self.assertEqual(401, response.status_code)

    def test_raises_error_when_the_product_ids_are_malformed(self):
        couple = factory.make_couple()
        gift_list = factory.make_gift_list(couple_id=couple.id)
        data = {
            "gift_list_id": gift_list.id,
            "products": [
                {"id": "2asdsad", "amount": 123}, {"id": 2, "amount": 3}
            ]
        }
        headers, _ = self.get_headers(couple.users[0])
        response = self.client.delete(
            self.url.format(gift_list_id=str(gift_list.id)),
            data=json.dumps(data), headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"][0],
            "Not a valid integer."
        )

    def test_raises_error_when_duplicated_product_ids_provided(self):
        couple = factory.make_couple()
        user = couple.users[0]
        gift_list = factory.make_gift_list(couple_id=str(couple.id))
        data = {
            "gift_list_id": str(gift_list.id),
            "products": [
                {"id": 2, "amount": 123}, {"id": 2, "amount": 3},
            ]
        }
        headers, _ = self.get_headers(user)
        response = self.client.delete(
            self.url.format(gift_list_id=str(gift_list.id)),
            data=json.dumps(data), headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"][0],
            AmendProductsSchema.errors["duplicates_found"]
        )

    def test_raises_error_when_the_product_ids_contain_more_ids_than_in_db(
            self):
        couple = factory.make_couple()
        gift_list = factory.make_gift_list(couple_id=couple.id)
        product = factory.make_product(in_stock_quantity=10)
        another_id = self.session.query(
            Product.id
        ).order_by(
            Product.id.desc()
        ).first()[0] + 10
        data = {
            "gift_list_id": gift_list.id,
            "products": [
                {"id": product.id, "amount": 1},
                {"id": another_id, "amount": 1}
            ]
        }
        headers, _ = self.get_headers(couple.users[0])
        response = self.client.delete(
            self.url.format(gift_list_id=str(gift_list.id)),
            data=json.dumps(data), headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"][0],
            AmendProductsSchema.errors["non_equal_prod_ids"]
        )

    def test_raises_when_a_user_tries_to_delete_to_another_list(self):
        couple = factory.make_couple()
        product = factory.make_product()
        gift_list = factory.make_gift_list(couple_id=couple.id)
        data = {
            "gift_list_id": gift_list.id,
            "products": [
                {"id": product.id, "amount": 1},
            ]
        }
        headers, _ = self.get_headers()
        response = self.client.delete(
            self.url.format(gift_list_id=gift_list.id),
            data=json.dumps(data), headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            response.json["message"],
            "This user hasn't got a gift list related."
        )

    def test_deletes_one_list_product_from_the_gift_list(self):
        initial_products = 4
        couple = factory.make_couple()
        user = couple.users[0]
        products = [factory.make_product() for _ in range(initial_products)]
        gift_list = factory.make_gift_list(couple_id=couple.id)
        for prod in products:
            factory.make_list_product(
                gift_list_id=gift_list.id, product_id=prod.id)
        data = {
            "gift_list_id": gift_list.id,
            "products": [
                {"id": str(products[0].id), "amount": 1},
            ]
        }
        headers, _ = self.get_headers(user)
        response = self.client.delete(
            self.url.format(gift_list_id=gift_list.id),
            data=json.dumps(data), headers=headers
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(len(gift_list.list_products), initial_products - 1)
        self.assertEqual(response.json["gift_list_id"], gift_list.id)

    def test_deletes_many_list_products_from_the_gift_list(self):
        initial_products = 4
        couple = factory.make_couple()
        user = couple.users[0]
        products = [factory.make_product() for _ in range(initial_products)]
        gift_list = factory.make_gift_list(couple_id=couple.id)
        for prod in products:
            factory.make_list_product(
                gift_list_id=gift_list.id, product_id=prod.id)
        products_to_delete = [
            {"id": str(prod.id), "amount": 1} for prod in products[:2]
        ]
        data = {
            "gift_list_id": gift_list.id,
            "products": products_to_delete
        }
        headers, _ = self.get_headers(user)
        response = self.client.delete(
            self.url.format(gift_list_id=gift_list.id),
            data=json.dumps(data), headers=headers
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(len(gift_list.list_products), initial_products - 2)
        self.assertEqual(response.json["gift_list_id"], gift_list.id)


class PurchaseProductTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.url = "/gift-list/{gift_list_id}/purchase/{product_id}"

    def test_returns_401_unauthorized_on_no_auth_headers(self):
        response = self.client.patch(
            self.url.format(gift_list_id=1, product_id=1))
        self.assertEqual(401, response.status_code)

    def test_returns_400_when_given_prod_is_not_related_with_the_list(
            self):
        gift_list = factory.make_gift_list()
        product = factory.make_product()
        headers, _ = self.get_headers()
        response = self.client.patch(
            self.url.format(
                gift_list_id=gift_list.id, product_id=product.id),
            headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            "The chosen product doesn't belong to this gift list",
            response.json["message"]
        )

    def test_returns_400_no_non_purchased_list_products_in_this_gift_list(
            self):
        couple = factory.make_couple()
        user = couple.users[0]
        gift_list = factory.make_gift_list(couple_id=couple.id)
        product = factory.make_product()
        for _ in range(5):
            factory.make_list_product(
                is_purchased=True, product_id=product.id,
                gift_list_id=gift_list.id)
        headers, _ = self.get_headers(user)
        response = self.client.patch(
            self.url.format(
                gift_list_id=gift_list.id, product_id=product.id),
            headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            "Product not in stock anymore.",
            response.json["message"]
        )

    def test_returns_400_when_no_stock_is_left_for_this_prod_in_this_gift_list(
            self):
        couple = factory.make_couple()
        user = couple.users[0]
        gift_list = factory.make_gift_list(couple_id=couple.id)
        product = factory.make_product(in_stock_quantity=0)
        for _ in range(5):
            factory.make_list_product(
                is_purchased=True, product_id=product.id,
                gift_list_id=gift_list.id)
        headers, _ = self.get_headers(user)
        response = self.client.patch(
            self.url.format(
                gift_list_id=gift_list.id, product_id=product.id),
            headers=headers
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            "Product not in stock anymore.",
            response.json["message"]
        )

    def test_returns_200_when_a_product_is_purchased(self):
        couple = factory.make_couple()
        user = couple.users[0]
        gift_list = factory.make_gift_list(couple_id=couple.id)
        product = factory.make_product()
        for _ in range(5):
            factory.make_list_product(
                product_id=product.id,
                gift_list_id=gift_list.id
            )
        headers, _ = self.get_headers(user)
        init_prod_total_purchased_quantity = product.total_purchased_quantity

        response = self.client.patch(
            self.url.format(
                gift_list_id=gift_list.id, product_id=product.id),
            headers=headers
        )
        purchased_list_products = [
            l_prod
            for l_prod in product.list_products
            if l_prod.is_purchased is True
        ]
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            product.total_purchased_quantity,
            init_prod_total_purchased_quantity + 1
        )
        self.assertEqual(1, len(purchased_list_products))
