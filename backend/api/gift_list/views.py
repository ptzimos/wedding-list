"""Gift list endpoints."""
from flasgger import swag_from
from flask import (
    Blueprint,
    request,
)
from flask_jwt_extended import (
    get_jwt_identity,
    jwt_required,
)
from marshmallow import ValidationError
from sqlalchemy import and_

from backend.api.decorators import ensure_gift_list_for_user
from backend.api.enums import ProductActionEnum
from backend.api.models import (
    GiftList,
    ListProduct,
    Product,
)
from backend.api.schemas import (
    CreateGiftListSchema,
    GiftListsSchema,
)
from backend.api.schemas import AmendProductForGiftListSchema
from backend.api.utils import parse_errors
from backend.base.database import db
from backend.base.utils import get_current_user

gift_list_api = Blueprint("gift-list", __name__)


@gift_list_api.route("/", methods=["get"])
@jwt_required
@swag_from("docs/get_gift_lists.yaml")
def get_gift_lists():
    """Return the gift lists for the given couple."""
    user = get_current_user(get_jwt_identity(), db.session)
    gift_lists = db.session.query(
        GiftList
    ).join(
        ListProduct
    ).filter(
        GiftList.couple_id == user.couple_id
    ).all()
    schema = GiftListsSchema()
    return schema.dump(gift_lists, many=True)


@gift_list_api.route("/create", methods=["post", ])
@jwt_required
@swag_from("docs/create_gift_list.yaml")
def create_gift_list():
    """Create a gift list, given the chosen products."""
    user = get_current_user(get_jwt_identity(), db.session)
    data = request.data.copy()
    data.update({
        "couple_id": user.couple_id,
    })
    schema = CreateGiftListSchema()
    try:
        loaded_data = schema.load(data)
    except ValidationError as err:
        return {"message": parse_errors(err.messages)}, 400
    gift_list_id = schema.save(loaded_data)
    return {"gift_list_id": gift_list_id}, 200


@gift_list_api.route("/<gift_list_id>/add-products", methods=["patch", ])
@jwt_required
@ensure_gift_list_for_user
@swag_from("docs/add_products_to_list.yaml")
def add_products_to_list(gift_list_id):
    """Add products to existing gift list."""
    data = request.data.copy()
    data.update({
        "gift_list_id": gift_list_id,
        "action": ProductActionEnum.add.value,
    })
    schema = AmendProductForGiftListSchema()
    try:
        loaded_data = schema.load(data)
    except ValidationError as err:
        return {"message": parse_errors(err.messages)}, 400
    gift_list_id = schema.save(loaded_data)
    return {"gift_list_id": gift_list_id}, 200


@gift_list_api.route("/<gift_list_id>/remove-products", methods=["delete", ])
@jwt_required
@ensure_gift_list_for_user
@swag_from("docs/remove_products_from_list.yaml")
def remove_products_from_list(gift_list_id):
    """Remove a product from the gift list."""
    data = request.data.copy()
    data.update({
        "gift_list_id": gift_list_id,
        "action": ProductActionEnum.remove.value,
    })
    schema = AmendProductForGiftListSchema()
    try:
        loaded_data = schema.load(data)
    except ValidationError as err:
        return {"message": parse_errors(err.messages)}, 400
    gift_list_id = schema.delete(loaded_data)
    return {"gift_list_id": gift_list_id}, 200


@gift_list_api.route("<gift_list_id>/purchase/<product_id>", methods=["patch"])
@jwt_required
@swag_from("docs/purchase_product.yaml")
def purchase_product(gift_list_id, product_id):
    """Purchase a product from a gift list."""
    product = db.session.query(
        Product
    ).join(
        ListProduct
    ).join(
        GiftList
    ).filter(
        and_(
            GiftList.id == gift_list_id,
            Product.id == product_id,
            ListProduct.gift_list_id == gift_list_id
        )
    ).first()
    if not product:
        msg = "The chosen product doesn't belong to this gift list"
        return {"message": msg}, 400

    if product.in_stock_quantity == 0:
        return {"message": "Product not in stock anymore."}, 400

    list_products = product.non_purchased_list_products(gift_list_id)
    if not list_products:
        return {"message": "Product not in stock anymore."}, 400

    list_product = list_products[0]
    product.in_stock_quantity -= 0
    product.total_purchased_quantity += 1
    list_product.is_purchased = True

    db.session.add_all([product, list_product])
    db.session.commit()
    return "", 200
