"""Products endpoints."""
from flasgger import swag_from
from flask import Blueprint
from flask_jwt_extended import jwt_required

from backend.api.models import Product
from backend.api.schemas import ProductSchema
from backend.base.database import db

products_api = Blueprint("products", __name__)


@products_api.route("/", methods=["get", ])
@jwt_required
@swag_from("docs/get_products.yaml")
def get_products():
    """Return the list of the products tha exist in the database."""
    products = db.session.query(Product).all()
    schema = ProductSchema()
    return schema.dump(products, many=True)
