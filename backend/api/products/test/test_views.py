"""Products views test cases."""
from backend.base import factory
from backend.base.testcases import TestCase


class GetProductsViewTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.url = "/products"

    def test_returns_401_unauthorized_on_no_auth_headers(self):
        response = self.client.get(self.url + "/")
        self.assertEqual(401, response.status_code)

    def test_returns_empty_dict_when_no_products_in_db(self):
        user = factory.make_user()
        headers = self.create_jwt_header_for_user(user.email)
        response = self.client.get(self.url + "/", headers=headers)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json, [])

    def test_return_all_products_from_db(self):
        items_num = 10
        for _ in range(items_num):
            factory.make_product()
        user = factory.make_user()
        headers = self.create_jwt_header_for_user(user.email)
        response = self.client.get(self.url + "/", headers=headers)
        data = response.json
        self.assertEqual(200, response.status_code)
        self.assertEqual(len(data), items_num)
