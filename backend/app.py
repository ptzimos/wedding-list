"""Module to start the app."""
from backend.api.base import app
from backend.api.constants import FLASK_ENV

if __name__ == "__main__":
    app.run(debug=FLASK_ENV == "dev")
