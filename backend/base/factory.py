from random import randint

from faker import Faker

from backend.base.database import db
from backend.api.models import (
    Couple,
    GiftList,
    ListProduct,
    User,
)
from backend.api.models import Product

fake = Faker()


def _get_value(name, fake_func=None, alt=None, **kwargs):
    """Function to return either a value produced by faker or the available one
    """
    if kwargs.get(name) is None:
        if alt is not None:
            return alt()
        return getattr(fake, fake_func)()
    return kwargs.get(name)


def make_user(**kwargs):
    """Create a user instance."""
    session = kwargs.get("session", db.session)
    instance = User(
        first_name=_get_value("first_name", fake_func="name", **kwargs),
        last_name=_get_value("last_name", fake_func="name", **kwargs),
        email=_get_value("email", fake_func="email", **kwargs),
        password=_get_value("password", alt=lambda: "password", **kwargs),
    )
    session.add(instance)
    session.commit()
    return instance


def make_couple(**kwargs):
    """Create a couple instance."""
    session = kwargs.get("session", db.session)

    instance = Couple()
    users = kwargs.get("users")

    if not users:
        users = [make_user() for _ in range(2)]
        instance.users = users
        session.add(instance)
    else:
        for user in users:
            user.couple = instance
    session.add_all([*users, instance])
    session.commit()
    return instance


def make_product(**kwargs):
    """Create a product instance."""
    session = kwargs.get("session", db.session)

    instance = Product(**{
        "name": _get_value("name", fake_func="name", **kwargs),
        "brand": _get_value("name", fake_func="name", **kwargs),
        "price": _get_value(
            "name", alt=lambda: str(randint(2, 200)) + "GBP", **kwargs),
        "currency": _get_value("currency", alt=lambda: "GBP", **kwargs),
        "in_stock_quantity": _get_value(
            "in_stock_quantity", alt=lambda: 10, **kwargs),
        "total_purchased_quantity": _get_value(
            "total_purchased_quantity", alt=lambda: 0, **kwargs),
    })
    session.add(instance)
    session.commit()
    return instance


def make_gift_list(**kwargs):
    """Create a GiftList instance."""
    session = kwargs.get("session", db.session)
    instance = GiftList(**{
        "couple_id": kwargs.pop("couple_id", None),
    })
    session.add(instance)
    session.commit()
    return instance


def make_list_product(**kwargs):
    """Make a list_product instance."""
    session = kwargs.get("session", db.session)
    instance = ListProduct(**{
        "product_id": _get_value(
            "product_id", alt=lambda: make_product().id, **kwargs),
        "gift_list_id": _get_value(
            "gift_list_id", alt=lambda: make_gift_list().id, **kwargs),
        "is_purchased": _get_value(
            "is_purchased", alt=lambda: False, **kwargs),
    })
    session.add(instance)
    session.commit()
    return instance
