"""Base utis functions for project wide usage."""
import os

from backend.api.constants import (
    FLASK_ENV,
    PROJECT_DIR,
    SORTED_TABLES_MAPPING,
)
from backend.base.database import db
from backend.api.models import User


def cleanup_db(session):
    """Delete all objects of all tables in the database."""
    session.rollback()
    session.close()
    with db.session.get_bind().begin() as transaction:
        for table_name in SORTED_TABLES_MAPPING.keys():
            result = transaction.execute(
                """SELECT EXISTS(
                SELECT 1 FROM information_schema.tables
                WHERE table_name = '{}'
                )""".format(table_name)).fetchall()
            if result[0][0]:
                transaction.execute(
                    """DELETE FROM "{}" CASCADE""".format(table_name))


def get_current_user(email, session):
    """Return the current user for email."""
    return session.query(User).filter(User.email == email).one_or_none()


def clear_logs():
    """Remove the logs file in dev mode, when restarting the flask app."""
    logs_file = os.path.join(PROJECT_DIR, "api.logs")
    if os.path.exists(logs_file) and FLASK_ENV == "dev":
        os.remove(logs_file)
