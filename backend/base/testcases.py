"""Base module for generic testcases."""
from unittest.mock import MagicMock

import testtools as testtools
from flask_jwt_extended import create_access_token
from flask_testing import TestCase as _TestCase

from backend.api.base import create_app
from backend.api.settings import TestingConfig
from backend.base import factory
from backend.base.database import db
from backend.base.utils import cleanup_db


class TestCase(testtools.TestCase, _TestCase):

    @staticmethod
    def create_jwt_header_for_user(email):
        """Create authorization header for jwt token."""
        val = f"Bearer {create_access_token(identity=email)}"
        return {"Authorization": val}

    def get_headers(self, user=None, json_headers=True):
        if not user:
            user = factory.make_user()
        headers = self.create_jwt_header_for_user(user.email)
        if json_headers:
            headers["content-type"] = "application/json"
        return headers, user

    def patch(self, obj, attribute, value=None):
        """Monkey-patch 'obj.attribute' to 'value' while the test is running.
        """
        if value is None:
            value = MagicMock()
        super().patch(obj, attribute, value)
        return value

    def create_app(self):
        return create_app(TestingConfig())

    def setUp(self):
        super().setUp()
        db.create_all()
        self.session = db.session

    def tearDown(self):
        cleanup_db(self.session)
        super().tearDown()
