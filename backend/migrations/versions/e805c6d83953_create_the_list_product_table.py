"""Create the list_product table.

Revision ID: e805c6d83953
Revises: 80c566d9056f
Create Date: 2020-09-13 13:32:11.313533

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "e805c6d83953"
down_revision = "80c566d9056f"
branch_labels = None
depends_on = None


def upgrade(engine_name):
    """Create the list_product table."""
    op.create_table(
        "list_product",
        sa.Column("id", postgresql.UUID(), nullable=False),
        sa.Column("gift_list_id", postgresql.UUID(), nullable=False),
        sa.Column("product_id", postgresql.INTEGER(), nullable=False),
        sa.Column("is_purchased", postgresql.BOOLEAN(), default=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_foreign_key(
        "fk_list_product_to_gift_list",
        "list_product", "gift_list",
        ["gift_list_id", ], ["id", ],
        ondelete="cascade",
    )
    op.create_foreign_key(
        "fk_list_product_to_product",
        "list_product", "product",
        ["product_id", ], ["id", ],
        ondelete="cascade",
    )


def downgrade(engine_name):
    """Drop the list_product table."""
    op.drop_table("list_product")
