"""Create the user table.

Revision ID: d94871b4155e
Revises: 
Create Date: 2020-09-10 23:55:58.834212

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from sqlalchemy_utils import PasswordType

revision = "d94871b4155e"
down_revision = None
branch_labels = None
depends_on = None


def upgrade(engine_name):
    """Create the user table."""
    op.create_table(
        "user",
        sa.Column("id", postgresql.UUID(), nullable=True),
        sa.Column(
            name="first_name",
            type_=sa.String(255),
            nullable=True,
        ),
        sa.Column(
            name="last_name",
            type_=sa.String(255),
            nullable=True,
        ),
        sa.Column(
            name="email",
            type_=sa.String(128),
            unique=True,
            nullable=True,
            default=None
        ),
        sa.Column(
            name="password",
            type_=PasswordType(),
            nullable=True,
        ),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade(engine_name):
    """Drop the user table."""
    op.drop_table("user")
