"""Create the gift_list table.

Revision ID: 80c566d9056f
Revises: 5e7ec7c6eb59
Create Date: 2020-09-13 06:24:31.592438

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "80c566d9056f"
down_revision = "5e7ec7c6eb59"
branch_labels = None
depends_on = None


def upgrade(engine_name):
    """Create the product table."""
    op.create_table(
        "gift_list",
        sa.Column("id", postgresql.UUID(), nullable=False),
        sa.Column("couple_id", postgresql.UUID(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_foreign_key(
        "fk_gift_list_to_couple",
        "gift_list", "couple",
        ["couple_id", ], ["id", ],
        ondelete="cascade",
    )


def downgrade(engine_name):
    """Drop the product table."""
    op.drop_table("gift_list")
