"""Create the product table.

Revision ID: 5e7ec7c6eb59
Revises: 56b086ca9dff
Create Date: 2020-09-13 05:06:50.061146

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "5e7ec7c6eb59"
down_revision = "56b086ca9dff"
branch_labels = None
depends_on = None


def upgrade(engine_name):
    """Create the product table."""
    op.create_table(
        "product",
        sa.Column("id", postgresql.INTEGER(), nullable=True),
        sa.Column("name", sa.String(), nullable=False),
        sa.Column("brand", sa.String(), nullable=False),
        sa.Column("price", sa.Float(), nullable=False),
        sa.Column("currency", sa.String(length=3), nullable=False),
        sa.Column("in_stock_quantity", sa.Integer(), nullable=False),
        sa.Column("total_purchased_quantity", sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade(engine_name):
    """Drop the product table."""
    op.drop_table("product")
