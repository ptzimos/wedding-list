"""Add couple FK at user table.

Revision ID: 56b086ca9dff
Revises: 8c197294ad55
Create Date: 2020-09-11 01:27:21.198693

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
from sqlalchemy.dialects.postgresql import UUID

revision = "56b086ca9dff"
down_revision = "8c197294ad55"
branch_labels = None
depends_on = None


def upgrade(engine_name):
    """Add couple_id column at the user table."""
    op.add_column(
        table_name="user",
        column=sa.Column(
            name="couple_id",
            type_=UUID,
            nullable=True,
        ),
    )
    op.create_foreign_key(
        "fk_user_to_couple",
        "user", "couple",
        ["couple_id", ], ["id", ],
        ondelete="SET NULL"
    )


def downgrade(engine_name):
    """Remove the couple_id column from user table."""
    op.drop_column(
        table_name="user",
        column_name="couple_id",
    )
