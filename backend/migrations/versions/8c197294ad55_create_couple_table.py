"""Created the couple model.

Revision ID: 8c197294ad55
Revises: d94871b4155e
Create Date: 2020-09-11 00:31:36.188223

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "8c197294ad55"
down_revision = "d94871b4155e"
branch_labels = None
depends_on = None


def upgrade(engine_name):
    """Create the couple table."""
    op.create_table(
        "couple",
        sa.Column("id", postgresql.UUID(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade(engine_name):
    """Drop the couple table."""
    op.drop_table("couple")
