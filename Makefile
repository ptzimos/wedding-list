backend_dir = $(PWD)/backend
python := $(PWD)/venv/bin/python
sed := sed -i.bak

venv:
	python3.6 -m venv venv
	@echo 'export PYTHONPATH=$(PWD)/backend:$$PYTHONPATH' >> venv/bin/activate

start-docker:
	docker-compose -f docker/docker-compose.dev.yaml up -d

stop-docker:
	docker-compose -f docker/docker-compose.dev.yaml down

test:
	cd $(PWD)/backend \
	&& rm .coverage \
	&& nosetests \
		--with-coverage \
		--cover-package=. \
		--cover-html \
		--cover-html-dir=../coverage_html

lint:
	@cd ${backend_dir} && flake8